@extends('layout/template')

@section('content')

 <h1>Test Work</h1>
 <a href="{{url('/books/create')}}" class="btn btn-success">Add new document</a>
 <hr>

 @if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <div class="row">
    @foreach ($books as $book)
      <div class="col-sm-3" style="border: 1px solid #000">
        <div class="card">
          <div class="card-body">
            <a data-lightbox="roadtrip" href="{{url($book->filename ? 'pdf_image/'.$book->filename: 'pdf_image/noimage.png')}}">
                <img src="{{url($book->filename ? 'pdf_image/'.$book->filename: 'pdf_image/noimage.png')}}" width="100%" class="img-responsive">
            </a>
            <h5 class="card-title">{{ $book->title }}</h5>
            <p class="card-text">{{ $book->isbn }} | {{ $book->author }} | {{ $book->publisher }}</p>
            <a href="{{url('books',$book->id)}}" style="width: 49%" class="btn btn-primary">Read</a>
            <a href="{{route('books.edit',$book->id)}}" style="width: 49%" class="btn btn-warning">Update</a>
            {!! Form::open(['method' => 'DELETE', 'route'=>['books.destroy', $book->id]]) !!}
             {!! Form::submit('Delete', ['class' => 'btn btn-danger delcenter']) !!}
             {!! Form::close() !!}

          </div>
        </div>
      </div>
    @endforeach

</div>
{{ $books->links() }}
@endsection