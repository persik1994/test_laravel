<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>BookStore</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap
	/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/lightbox.css') }}">
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/lightbox.min.js') }}"></script>
</body>
</html>