<?php

namespace App;

use App\Traits\HelperTrait;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'isbn',
        'title',
        'author',
        'publisher',
        'image',
        'filename',
    ];
    public $timestamps = false;
}
