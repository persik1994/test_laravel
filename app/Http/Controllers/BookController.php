<?php
namespace App\Http\Controllers;
use App\Book;
use Request;
use Illuminate\Database\Eloquent\Model;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Validator;
use Imagick;
use View;
use App\Http\Requests;

class BookController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return Response
    */
   public function index()
   {
        $books=Book::paginate(20);
        return view('books.index',compact('books'));
   }
   /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
   public function create()
   {
		return view('books.create');
   }
   /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
   public function store(Request $request)
   {
	   // $book = Request::all();
     $convert_from_pdf_to_jpg = str_replace('.pdf', '.jpg', Request::file('image')->getClientOriginalName());

	   $file = Request::file('image');
	   $file_name = $file->getClientOriginalName();
	   $file->move(public_path('img/'), $file_name);

     $pdf = public_path('img/') . $file_name . '[0]';
     $imagick = new Imagick($pdf); 
     $imagick->setImageFormat('jpg');
     $pdf_file_name = public_path('pdf_image/') . str_replace('.pdf', '.jpg' ,$file_name);
     $imagick->writeImage($pdf_file_name);

	   Book::create([
          'isbn' => Request::get('isbn'),
          'title' => Request::get('title'),
          'author' => Request::get('author'),
          'publisher' => Request::get('publisher'),
          'image' => $file_name,
          'filename' => $convert_from_pdf_to_jpg,
     ]);

	   return redirect('books');
   }
   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function show($id)
   {
	   $book=Book::find($id);
	   return view('books.show',compact('book'));
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
   public function edit($id)
   {
	   $book=Book::find($id);
	   return view('books.edit',compact('book'));
   }
   /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function update($id)
   {
	   $bookUpdate=Request::all();
	   $book=Book::find($id);
	   $book->update($bookUpdate);
	   return redirect('books');
   }
   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
   public function destroy($id)
   {
	   Book::find($id)->delete();
	   return redirect('books');
   }
}
